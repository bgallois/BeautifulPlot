beautifulplot package
=====================

Submodules
----------

beautifulplot.BarChart module
-----------------------------

.. automodule:: beautifulplot.BarChart
    :members:
    :undoc-members:
    :show-inheritance:

beautifulplot.BoxPlot module
----------------------------

.. automodule:: beautifulplot.BoxPlot
    :members:
    :undoc-members:
    :show-inheritance:

beautifulplot.ScatterPlot module
--------------------------------

.. automodule:: beautifulplot.ScatterPlot
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: beautifulplot
    :members:
    :undoc-members:
    :show-inheritance:
