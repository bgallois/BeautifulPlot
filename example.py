'''
This file contains example of how to plot scatter plot, bar chart and boxplot with beautifulplot
'''

import beautifulplot as bp # import beautiful plot.
import numpy as np


# generate some random data 
data = []
for i in range(5):
    data.append(np.random.normal(np.random.randint(1, 5), np.random.randint(1,5)/10, 100))



fig1 = bp.BarChart() # Initialize bar chart
fig1.plot(data, color = ['b', 'r', 'y', 'g', 'm'], label = ['dat1', 'dat2', 'dat3', 'dat4', 'dat5']) # Plot the bar chart
fig1.addLabels(ylabel = 'Intensity') # Add ylabel
fig1.addTitle('A beautiful Bar Chart', size = 20) # Add title
fig1.addN(size = 12) # Add number of elements in each bar
for i in range(2, 6): # Add p-value
    fig1.addPvalueBar(1, i, test = 'Unpaired t test')
fig1.plotPoints(size = 2) # Plot point inside the bar


x = np.linspace(1,10000,100)
y = np.zeros(len(x))
for i in range(len(y)):
    y[i] = 2*x[i] + np.random.rand(1)*10000

fig2 = bp.ScatterPlot()
fig2.plot([x, y], legend = 'dat1')
fig2.plot([y, x], legend = 'dat2')
fig2.plot([x, x], legend = 'dat3')
fig2.addLabels(xlabel = 'lenght', ylabel = 'weight')
fig2.fitting([x, y], equation = 'linear')
fig2.fitting([y, x], equation = 'linear')


fig3 = bp.BoxPlot() # Initialize boxplot
fig3.plot(data, color = ['b', 'r', 'y', 'g', 'm'], label = ['dat1', 'dat2', 'dat3', 'dat4', 'dat5']) # Plot the boxplot
fig3.addLabels(ylabel = 'Intensity') # Add ylabel
fig3.addTitle('A beautiful Bar Chart', size = 20) # Add title
fig3.addN(size = 12) # Add numbver of elements in each bar
for i in range(2, 6): # Add p-value
    fig3.addPvalueBar(1, i, test = 'Unpaired t test')
fig3.plotPoints(size = 2) # Plot point inside the box


bp.show()